package main

import (
  "datamahasiswa/mahasiswa"
  "datamahasiswa/utils"
  "datamahasiswa/models"
  "context"
  "fmt"
  "log" 
  "net/http"
  "encoding/json"
  "github.com/julienschmidt/httprouter"
)

//-------------------------------------------------------Contoh format JSON untuk post data nilai mahasiswa : ----------------
// {
//    "nama":"Jedidiah",
//    "matakuliah":"Biologi",
//    "nilai":65
// }
//Indeks nilai sudah ditentukan di repository_mysql.go line 73


func main() {
  router := httprouter.New()
  router.GET("/", GetDataNilai)
  router.POST("/masukkandata", PostDataNilai)
  router.PUT("/u/:id", UpdateDataNilai)
  router.DELETE("/d/:id", DeleteDataNilai)

  fmt.Println("Server Running at http://localhost:8080")
  log.Fatal(http.ListenAndServe(":8080", router))

}

// Read
// GetMovie
func GetDataNilai(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  kumpulanNilai, err := mahasiswa.GetAll(ctx)

  if err != nil {
    fmt.Println(err)
  }

  utils.ResponseJSON(w, kumpulanNilai, http.StatusOK)
}

// Create
// PostMovie
func PostDataNilai(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
  if r.Header.Get("Content-Type") != "application/json" {
    http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
    return
  }
  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  var mah models.NilaiMahasiswa
  if err := json.NewDecoder(r.Body).Decode(&mah); err != nil {
    utils.ResponseJSON(w, err, http.StatusBadRequest)
    return
  }
  if err := mahasiswa.Insert(ctx, mah); err != nil {
    utils.ResponseJSON(w, err, http.StatusInternalServerError)
    return
  }
  res := map[string]string{
    "status": "Succesfully",
  }
  utils.ResponseJSON(w, res, http.StatusCreated)

}

// Update
// UpdateMovie
func UpdateDataNilai(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
  if r.Header.Get("Content-Type") != "application/json" {
    http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
    return
  }

  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()

  var mah models.NilaiMahasiswa

  if err := json.NewDecoder(r.Body).Decode(&mah); err != nil {
    utils.ResponseJSON(w, err, http.StatusBadRequest)
    return
  }

  var idMovie = ps.ByName("id")

  if err := mahasiswa.Update(ctx, mah, idMovie); err != nil {
    utils.ResponseJSON(w, err, http.StatusInternalServerError)
    return
  }

  res := map[string]string{
    "status": "Succesfully",
  }

  utils.ResponseJSON(w, res, http.StatusCreated)
}

// Delete
// DeleteMovie
func DeleteDataNilai(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  var idMovie = ps.ByName("id")
  if err := mahasiswa.Delete(ctx, idMovie); err != nil {
    kesalahan := map[string]string{
      "error": fmt.Sprintf("%v", err),
    }
    utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
    return
  }
  res := map[string]string{
    "status": "Succesfully",
  }
  utils.ResponseJSON(w, res, http.StatusOK)
}
