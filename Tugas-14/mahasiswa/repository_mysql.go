package mahasiswa

import (
  "database/sql"
  "datamahasiswa/config"
  "datamahasiswa/models"
  "context"
  "fmt"
  "log"
  "time"
  "errors"
)

const (
  table          = "mahasiswa"
  layoutDateTime = "2006-01-02 15:04:05"
)

// GetAll
func GetAll(ctx context.Context) ([]models.NilaiMahasiswa, error) {
  var kumpulanNilai []models.NilaiMahasiswa
  db, err := config.MySQL()

  if err != nil {
    log.Fatal("Cant connect to MySQL", err)
  }

  queryText := fmt.Sprintf("SELECT * FROM %v Order By created_at DESC", table)
  rowQuery, err := db.QueryContext(ctx, queryText)

  if err != nil {
    log.Fatal(err)
  }

  for rowQuery.Next() {
    var mahasiswa models.NilaiMahasiswa
    var createdAt, updatedAt string
    if err = rowQuery.Scan(&mahasiswa.ID,
      &mahasiswa.Nama,
      &mahasiswa.MataKuliah,
      &mahasiswa.IndeksNilai,
      &mahasiswa.Nilai,
      &createdAt,
      &updatedAt); err != nil {
      return nil, err
    }

    //  Change format string to datetime for created_at and updated_at
    mahasiswa.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

    if err != nil {
      log.Fatal(err)
    }

    mahasiswa.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
    if err != nil {
      log.Fatal(err)
    }

    kumpulanNilai = append(kumpulanNilai, mahasiswa)
  }
  return kumpulanNilai, nil
}

// Insert NilaiMahasiswa
func Insert(ctx context.Context, mahasiswa models.NilaiMahasiswa) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't connect to MySQL", err)
  }
  

  mahasiswa.IndeksNilai = ""
    if mahasiswa.Nilai >= 80 {
      mahasiswa.IndeksNilai = "A"
    } else if mahasiswa.Nilai >= 70 {
      mahasiswa.IndeksNilai = "B"
    } else if mahasiswa.Nilai >= 60 {
      mahasiswa.IndeksNilai = "C"
    } else if mahasiswa.Nilai >= 50 {
      mahasiswa.IndeksNilai = "D"
    } else if mahasiswa.Nilai < 50 {
      mahasiswa.IndeksNilai = "E"
    }

  queryText := fmt.Sprintf("INSERT INTO %v (nama, matakuliah,indeksnilai, nilai, created_at, updated_at) values('%v','%v','%v',%v, NOW(), NOW())", table,
    mahasiswa.Nama,
    mahasiswa.MataKuliah,
    mahasiswa.IndeksNilai,
    mahasiswa.Nilai,
  )
  _, err = db.ExecContext(ctx, queryText)

  if err != nil {
    return err
  }
  return nil
}

// Update NilaiMahasiswa
func Update(ctx context.Context, mahasiswa models.NilaiMahasiswa, id string) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't connect to MySQL", err)
  }

  queryText := fmt.Sprintf("UPDATE %v set nama ='%s', matakuliah ='%s', nilai = %d, updated_at = NOW() where id = %s",
    table,
    mahasiswa.Nama,
    mahasiswa.MataKuliah,
    mahasiswa.Nilai,
    id,
  )

  _, err = db.ExecContext(ctx, queryText)
  if err != nil {
    return err
  }

  return nil
}

// Delete NilaiMahasiswa
func Delete(ctx context.Context, id string) error {
    db, err := config.MySQL()

    if err != nil {
        log.Fatal("Can't connect to MySQL", err)
    }

    queryText := fmt.Sprintf("DELETE FROM %v where id = %s", table, id)

    s, err := db.ExecContext(ctx, queryText)

    if err != nil && err != sql.ErrNoRows {
        return err
    }

    check, err := s.RowsAffected()
    fmt.Println(check)
    if check == 0 {
        return errors.New("id tidak ada")
    }

    if err != nil {
        fmt.Println(err.Error())
    }

    return nil
}