package models

import (
  "time"
)

type (
  // Movie
  NilaiMahasiswa struct {
  Nama string `json:"nama"`
  MataKuliah string `json:"matakuliah"`
  IndeksNilai string
  Nilai uint `json:"nilai"`
  ID uint
  CreatedAt time.Time `json:"created_at"`
  UpdatedAt time.Time `json:"updated_at"`
  }
)