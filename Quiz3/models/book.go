package models

import (
  "time"
)

type (
  // Movie
  Book struct {
  ID uint
  Title string `json:"title"`
  Description string `json:"description"`
  Image_url string `json:"image_url"`
  Release_year int `json:"release_year"`
  Price string `json:"price"`
  Total_page int `json:"total_page"`
  Thickness string `json:"thickness"`
  CreatedAt time.Time `json:"created_at"`
  UpdatedAt time.Time `json:"updated_at"`
  Category_id int `json:"category_id"`
  }
)