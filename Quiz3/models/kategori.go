package models

import (
  "time"
)

type (
  // Movie
  Kategori struct {
  ID uint
  Nama string `json:"nama"`
  CreatedAt time.Time `json:"created_at"`
  UpdatedAt time.Time `json:"updated_at"`
  }
)