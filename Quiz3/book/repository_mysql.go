package book

import (
  "database/sql"
  "quiz3/config"
  "quiz3/models"
  "context"
  "fmt"
  "log"
  "strconv"
  "time"

  //net/url digunakan untuk validasi image_url. Net/url termasuk standard package Golang.
  "net/url"
  "errors"
)

const (
  table          = "book"
  layoutDateTime = "2006-01-02 15:04:05"
)

// GetAll
func GetAll(ctx context.Context, title string, minYear int, maxYear int, minPage int, maxPage int, sortByTitle string) ([]models.Book, error) {
  var kumpulanBook []models.Book
  db, err := config.MySQL()

  if err != nil {
    log.Fatal("Cant connect to MySQL", err)
  }

  addwhere := " WHERE "
  if title != "" || minYear != 0 || maxYear != 0 || minPage != 0 || maxPage != 0 {
  addwhere = " WHERE "
  }

  wheretitle := "1=1"
  if title != "" {
    wheretitle = ` title LIKE "%%` + title + `%%" ` 
  }

  whereminyear := ""
  if minYear != 0 {
    whereminyear = " AND release_year>" + strconv.Itoa(minYear)
  }

  wheremaxyear := ""
  if maxYear != 0 {
    wheremaxyear = " AND release_year<" + strconv.Itoa(maxYear)
  }

  whereminpage := ""
  if minPage != 0 {
    whereminpage = " AND total_page>" + strconv.Itoa(minPage)
  }

  wheremaxpage := ""
  if maxPage != 0 {
    wheremaxpage = " AND total_page>" + strconv.Itoa(maxPage)
  } 

  sorttitle := " ORDER BY created_at DESC"
  if sortByTitle != "" {
    sorttitle = " ORDER BY title " + sortByTitle
  }

  queryText := fmt.Sprintf("SELECT * FROM %v " + addwhere + wheretitle + whereminyear + wheremaxyear + whereminpage + wheremaxpage + sorttitle, table)
  rowQuery, err := db.QueryContext(ctx, queryText)

  if err != nil {
    log.Fatal(err)
  }

  for rowQuery.Next() {
    var book models.Book
    var createdAt, updatedAt string
    if err = rowQuery.Scan(&book.ID,
    &book.Title,
    &book.Description,
    &book.Image_url,
    &book.Release_year,
    &book.Price,
    &book.Total_page,
    &book.Thickness,
    &createdAt,
    &updatedAt,
    &book.Category_id,); err != nil {
      return nil, err
    }

    //  Change format string to datetime for created_at and updated_at
    book.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

    if err != nil {
      log.Fatal(err)
    }

    book.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
    if err != nil {
      log.Fatal(err)
    }

    kumpulanBook = append(kumpulanBook, book)
  }
  return kumpulanBook, nil
}

// Insert Nilaibook
func Insert(ctx context.Context, book models.Book) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't connect to MySQL", err)
  }

  //Release_year maksimal 2021 minimal 1980 :
  if book.Release_year < 1980 || book.Release_year > 2021 {
   log.Fatal("Tahun tidak diantara 1980 - 2021.", err)
  }

  // Validasi image_url :
  u, err := url.ParseRequestURI(book.Image_url)
   if err != nil {
   log.Fatal( u , "Image url tidak mengikuti format url." , err)
}
  // Menentukan Thickness :
    book.Thickness = ""
    if book.Total_page >= 201 {
      book.Thickness = "Tebal"
    } else if book.Total_page <= 100 {
      book.Thickness = "Tipis"
    } else if book.Total_page <= 200 {
      book.Thickness = "Sedang"
    }
     

  queryText := fmt.Sprintf("INSERT INTO %v (title, description, image_url, release_year, price, total_page, thickness, category_id, created_at, updated_at) values ('%v','%v','%v',%v,'%v',%v,'%v',%v, NOW(), NOW())", table,
    book.Title,
    book.Description,
    book.Image_url,
    book.Release_year,
    book.Price,
    book.Total_page,
    book.Thickness,
    book.Category_id,
  )
  _, err = db.ExecContext(ctx, queryText)

  if err != nil {
    return err
  }
  return nil
}

// Update Nilaibook
func Update(ctx context.Context, book models.Book, id string) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't connect to MySQL", err)
  }

  book.Thickness = ""
    if book.Total_page >= 201 {
      book.Thickness = "Tebal"
    } else if book.Total_page <= 100 {
      book.Thickness = "Tipis"
    } else if book.Total_page <= 200 {
      book.Thickness = "Sedang"
    }

  queryText := fmt.Sprintf("UPDATE %v set title ='%v', description ='%v', image_url ='%v',release_year ='%v',  price ='%v', total_page ='%v',thickness ='%v',category_id ='%v',updated_at = NOW() where id = %s",
    table,
    book.Title,
    book.Description,
    book.Image_url,
    book.Release_year,
    book.Price,
    book.Total_page,
    book.Thickness,
    book.Category_id,
    id,
  )

  _, err = db.ExecContext(ctx, queryText)
  if err != nil {
    return err
  }

  return nil
}

// Delete Nilaibook
func Delete(ctx context.Context, id string) error {
    db, err := config.MySQL()

    if err != nil {
        log.Fatal("Can't connect to MySQL", err)
    }

    queryText := fmt.Sprintf("DELETE FROM %v where id = %s", table, id)

    s, err := db.ExecContext(ctx, queryText)

    if err != nil && err != sql.ErrNoRows {
        return err
    }

    check, err := s.RowsAffected()
    fmt.Println(check)
    if check == 0 {
        return errors.New("id tidak ada")
    }

    if err != nil {
        fmt.Println(err.Error())
    }

    return nil
}