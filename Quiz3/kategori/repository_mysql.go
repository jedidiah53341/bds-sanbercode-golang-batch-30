package kategori

import (
  "database/sql"
  "quiz3/config"
  "quiz3/models"
  "context"
  "fmt"
  "log"
  "time"
  "errors"
)

const (
  table          = "category"
  layoutDateTime = "2006-01-02 15:04:05"
)

// GetAll
func GetAll(ctx context.Context) ([]models.Kategori, error) {
  var kumpulanNilai []models.Kategori
  db, err := config.MySQL()

  if err != nil {
    log.Fatal("Cant connect to MySQL", err)
  }

  queryText := fmt.Sprintf("SELECT * FROM %v Order By created_at DESC", table)
  rowQuery, err := db.QueryContext(ctx, queryText)

  if err != nil {
    log.Fatal(err)
  }

  for rowQuery.Next() {
    var kategori models.Kategori
    var createdAt, updatedAt string
    if err = rowQuery.Scan(&kategori.ID,
      &kategori.Nama,
      &createdAt,
      &updatedAt); err != nil {
      return nil, err
    }

    //  Change format string to datetime for created_at and updated_at
    kategori.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

    if err != nil {
      log.Fatal(err)
    }

    kategori.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
    if err != nil {
      log.Fatal(err)
    }

    kumpulanNilai = append(kumpulanNilai, kategori)
  }
  return kumpulanNilai, nil
}

// Insert NilaiMahasiswa
func Insert(ctx context.Context, kategori models.Kategori) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't connect to MySQL", err)
  }
  

  queryText := fmt.Sprintf("INSERT INTO %v (name, created_at, updated_at) values('%v', NOW(), NOW())", table,
    kategori.Nama,
  )
  _, err = db.ExecContext(ctx, queryText)

  if err != nil {
    return err
  }
  return nil
}

// Update NilaiMahasiswa
func Update(ctx context.Context, kategori models.Kategori, id string) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't connect to MySQL", err)
  }

  queryText := fmt.Sprintf("UPDATE %v set name ='%s', updated_at = NOW() where id = %s",
    table,
    kategori.Nama,
    id,
  )

  _, err = db.ExecContext(ctx, queryText)
  if err != nil {
    return err
  }

  return nil
}

// Delete NilaiMahasiswa
func Delete(ctx context.Context, id string) error {
    db, err := config.MySQL()

    if err != nil {
        log.Fatal("Can't connect to MySQL", err)
    }

    queryText := fmt.Sprintf("DELETE FROM %v where id = %s", table, id)

    s, err := db.ExecContext(ctx, queryText)

    if err != nil && err != sql.ErrNoRows {
        return err
    }

    check, err := s.RowsAffected()
    fmt.Println(check)
    if check == 0 {
        return errors.New("id tidak ada")
    }

    if err != nil {
        fmt.Println(err.Error())
    }

    return nil
}