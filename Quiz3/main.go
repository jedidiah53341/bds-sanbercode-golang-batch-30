package main

import (
  "quiz3/kategori"
  "quiz3/book"
  "quiz3/utils"
  "quiz3/models"
  "context"
  "fmt"
  "log" 
  "net/http"
  "encoding/json"
  "strconv"
  "github.com/julienschmidt/httprouter"
)


func main() {
  router := httprouter.New()


  //Routes Kategori
  router.GET("/categories", GetKategori)
  router.POST("/categories", PostKategori)
  router.PUT("/categories/:id", UpdateKategori)
  router.DELETE("/categories/:id", DeleteKategori)

  //Routes Buku
  router.GET("/books", GetBook)
  router.POST("/books", PostBook)
  router.PUT("/books/:id", UpdateBook)
  router.DELETE("/books/:id", DeleteBook)

  fmt.Println("Server Running at http://localhost:8080")
  log.Fatal(http.ListenAndServe(":8080", router))

}

//Basic Auth ------------------------------------------------------------------------------------------------

//Kategori----------------------------------------------------------------------------------------------------
// Read
func GetKategori(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {


  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  kumpulanKategori, err := kategori.GetAll(ctx)

  if err != nil {
    fmt.Println(err)
  }


  utils.ResponseJSON(w, kumpulanKategori, http.StatusOK)
}

// Create
func PostKategori(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

  // Basic Auth
  uname, pwd, ok := r.BasicAuth()
    if !ok {
      w.Write([]byte("Username atau Password tidak boleh kosong"))
      return
    }

    if !(uname == "admin" && pwd == "password") && !(uname == "editor" && pwd == "secret") && !(uname == "trainer" && pwd == "rahasia") {
      w.Write([]byte("Username atau Password tidak sesuai"))
      return
   }
    
  if r.Header.Get("Content-Type") != "application/json" {
    http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
    return
  }
  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  var kat models.Kategori
  if err := json.NewDecoder(r.Body).Decode(&kat); err != nil {
    utils.ResponseJSON(w, err, http.StatusBadRequest)
    return
  }
  if err := kategori.Insert(ctx, kat); err != nil {
    utils.ResponseJSON(w, err, http.StatusInternalServerError)
    return
  }
  res := map[string]string{
    "status": "Succesfully",
  }
  utils.ResponseJSON(w, res, http.StatusCreated)

}

// Update
func UpdateKategori(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

  // Basic Auth
  uname, pwd, ok := r.BasicAuth()
    if !ok {
      w.Write([]byte("Username atau Password tidak boleh kosong"))
      return
    }

    if !(uname == "admin" && pwd == "password") && !(uname == "editor" && pwd == "secret") && !(uname == "trainer" && pwd == "rahasia") {
      w.Write([]byte("Username atau Password tidak sesuai"))
      return
   }


  if r.Header.Get("Content-Type") != "application/json" {
    http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
    return
  }

  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()

  var kat models.Kategori

  if err := json.NewDecoder(r.Body).Decode(&kat); err != nil {
    utils.ResponseJSON(w, err, http.StatusBadRequest)
    return
  }

  var idMovie = ps.ByName("id")

  if err := kategori.Update(ctx, kat, idMovie); err != nil {
    utils.ResponseJSON(w, err, http.StatusInternalServerError)
    return
  }

  res := map[string]string{
    "status": "Succesfully",
  }

  utils.ResponseJSON(w, res, http.StatusCreated)
}

// Delete
func DeleteKategori(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

  // Basic Auth
  uname, pwd, ok := r.BasicAuth()
    if !ok {
      w.Write([]byte("Username atau Password tidak boleh kosong"))
      return
    }

    if !(uname == "admin" && pwd == "password") && !(uname == "editor" && pwd == "secret") && !(uname == "trainer" && pwd == "rahasia") {
      w.Write([]byte("Username atau Password tidak sesuai"))
      return
   }


  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  var idMovie = ps.ByName("id")
  if err := kategori.Delete(ctx, idMovie); err != nil {
    kesalahan := map[string]string{
      "error": fmt.Sprintf("%v", err),
    }
    utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
    return
  }
  res := map[string]string{
    "status": "Succesfully",
  }
  utils.ResponseJSON(w, res, http.StatusOK)
}



//Book----------------------------------------------------------------------------------------------------
// Read
func GetBook(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

  title := ""
  title_s, ok := r.URL.Query()["title"]
    if !ok || len(title_s[0]) < 1 {
        log.Println("Url Param 'title' is missing")
    } else { title = title_s[0] }

  minYear := "0"
  minYear_s, ok := r.URL.Query()["minYear"]
    if !ok || len(minYear_s[0]) < 1 {
        log.Println("Url Param 'minYear' is missing")
    } else { minYear = minYear_s[0] }
    int_minYear, err := strconv.Atoi(minYear)
    if err != nil {
        // handle error
        fmt.Println(err)
    } 

    maxYear := "0"
    maxYear_s, ok := r.URL.Query()["maxYear"]
    if !ok || len(maxYear_s[0]) < 1 {
        log.Println("Url Param 'maxYear' is missing")
    } else { maxYear = maxYear_s[0] }
    int_maxYear, err := strconv.Atoi(maxYear)
    if err != nil {
        // handle error
        fmt.Println(err)
    }

    minPage := "0"
    minPage_s, ok := r.URL.Query()["minPage"]
    if !ok || len(minPage_s[0]) < 1 {
        log.Println("Url Param 'minPage' is missing")
    } else { minPage = minPage_s[0] }
    int_minPage, err := strconv.Atoi(minPage)
    if err != nil {
        // handle error
        fmt.Println(err)
    }
    
    maxPage := "0"
    maxPage_s, ok := r.URL.Query()["maxPage"]
    if !ok || len(maxPage_s[0]) < 1 {
        log.Println("Url Param 'maxPage' is missing")
    } else { maxPage = maxPage_s[0] }
    int_maxPage, err := strconv.Atoi(maxPage)
    if err != nil {
        // handle error
        fmt.Println(err)
    }

    sortByTitle := ""
    sortByTitle_s, ok := r.URL.Query()["sortByTitle"]
    if !ok || len(sortByTitle_s[0]) < 1 {
        log.Println("Url Param 'sortByTitle' is missing")
    } else {
      sortByTitle = sortByTitle_s[0] }

  //-----------------------------------


  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  kumpulanbook, err := book.GetAll(ctx, title, int_minYear, int_maxYear, int_minPage, int_maxPage, sortByTitle)

  if err != nil {
    fmt.Println(err)
  }

  utils.ResponseJSON(w, kumpulanbook, http.StatusOK)
}

// Create
func PostBook(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

  // Basic Auth
  uname, pwd, ok := r.BasicAuth()
    if !ok {
      w.Write([]byte("Username atau Password tidak boleh kosong"))
      return
    }

    if !(uname == "admin" && pwd == "password") && !(uname == "editor" && pwd == "secret") && !(uname == "trainer" && pwd == "rahasia") {
      w.Write([]byte("Username atau Password tidak sesuai"))
      return
   }

  if r.Header.Get("Content-Type") != "application/json" {
    http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
    return
  }
  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  var boo models.Book
  if err := json.NewDecoder(r.Body).Decode(&boo); err != nil {
    utils.ResponseJSON(w, err, http.StatusBadRequest)
    return
  }
  if err := book.Insert(ctx, boo); err != nil {
    utils.ResponseJSON(w, err, http.StatusInternalServerError)
    return
  }
  res := map[string]string{
    "status": "Succesfully",
  }
  utils.ResponseJSON(w, res, http.StatusCreated)

}

// Update
func UpdateBook(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

  // Basic Auth
  uname, pwd, ok := r.BasicAuth()
    if !ok {
      w.Write([]byte("Username atau Password tidak boleh kosong"))
      return
    }

    if !(uname == "admin" && pwd == "password") && !(uname == "editor" && pwd == "secret") && !(uname == "trainer" && pwd == "rahasia") {
      w.Write([]byte("Username atau Password tidak sesuai"))
      return
   }

  if r.Header.Get("Content-Type") != "application/json" {
    http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
    return
  }

  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()

  var boo models.Book

  if err := json.NewDecoder(r.Body).Decode(&boo); err != nil {
    utils.ResponseJSON(w, err, http.StatusBadRequest)
    return
  }

  var idMovie = ps.ByName("id")

  if err := book.Update(ctx, boo, idMovie); err != nil {
    utils.ResponseJSON(w, err, http.StatusInternalServerError)
    return
  }

  res := map[string]string{
    "status": "Succesfully",
  }

  utils.ResponseJSON(w, res, http.StatusCreated)
}

// Delete
func DeleteBook(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

  // Basic Auth
  uname, pwd, ok := r.BasicAuth()
    if !ok {
      w.Write([]byte("Username atau Password tidak boleh kosong"))
      return
    }

    if !(uname == "admin" && pwd == "password") && !(uname == "editor" && pwd == "secret") && !(uname == "trainer" && pwd == "rahasia") {
      w.Write([]byte("Username atau Password tidak sesuai"))
      return
   }


  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  var idMovie = ps.ByName("id")
  if err := book.Delete(ctx, idMovie); err != nil {
    kesalahan := map[string]string{
      "error": fmt.Sprintf("%v", err),
    }
    utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
    return
  }
  res := map[string]string{
    "status": "Succesfully",
  }
  utils.ResponseJSON(w, res, http.StatusOK)
}