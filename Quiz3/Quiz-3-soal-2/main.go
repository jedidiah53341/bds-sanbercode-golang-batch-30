package main

import (
    "log"
    "net/http"
    "fmt"
    "strconv"
    "math"
)

func main() {
    http.HandleFunc("/bangun-datar/segitiga-sama-sisi", segitigaSamaSisi)
    http.HandleFunc("/bangun-datar/persegi", persegi)
    http.HandleFunc("/bangun-datar/persegi-panjang", persegiPanjang)
    http.HandleFunc("/bangun-datar/lingkaran", lingkaran)
    http.HandleFunc("/bangun-datar/jajar-genjang", jajarGenjang)
    fmt.Println("Server Running at http://localhost:8080")
    http.ListenAndServe(":8080", nil)
      
}

//Segitiga Sama Sisi---------------------------------------------------------------------------------
func segitigaSamaSisi(w http.ResponseWriter, r *http.Request) {

    hitung_s, ok := r.URL.Query()["hitung"]
    if !ok || len(hitung_s[0]) < 1 {
        log.Println("Url Param 'hitung' is missing")
        return
    }
    hitung := hitung_s[0]

    alas_s, ok := r.URL.Query()["alas"]
    if !ok || len(alas_s[0]) < 1 {
        log.Println("Url Param 'alas' is missing")
        return
    }
    alas := alas_s[0]
    int_alas, err := strconv.Atoi(alas)
    if err != nil {
        // handle error
        fmt.Println(err)
    }

    tinggi_s, ok := r.URL.Query()["tinggi"]
    if !ok || len(tinggi_s[0]) < 1 {
        log.Println("Url Param 'tinggi' is missing")
        return
    }
    tinggi := tinggi_s[0]
    int_tinggi, err := strconv.Atoi(tinggi)
    if err != nil {
        // handle error
        fmt.Println(err)
    }

    log.Println("Url Param 'alas' is: " + string(alas))
    log.Println("Url Param 'tinggi' is: " + string(tinggi))
    log.Println("Url Param 'hitung' is: " + string(hitung))

    var luasSegitigaSamaSisi = func (sebuahChannelUntukMenampungHasilPerhitunganSoal4 chan<- int, alas int, tinggi int) {
  sebuahChannelUntukMenampungHasilPerhitunganSoal4 <- (alas*tinggi/2)
}
    var kelilingSegitigaSamaSisi = func (sebuahChannelUntukMenampungHasilPerhitunganSoal4 chan<- int, alas int, tinggi int) {
  sebuahChannelUntukMenampungHasilPerhitunganSoal4 <- (alas*3)
}
    var ch1 = make(chan int)
    if hitung == "luas" {
    go luasSegitigaSamaSisi(ch1, int_alas,int_tinggi)
 fmt.Println("luas segitiga sama sisi adalah :", <-ch1)
} else if hitung == "keliling" {
    go kelilingSegitigaSamaSisi(ch1, int_alas,int_tinggi)
 fmt.Println("keliling segitiga sama sisi adalah :", <-ch1)
}
}


//Persegi---------------------------------------------------------------------------------
func persegi(w http.ResponseWriter, r *http.Request) {

    hitung_s, ok := r.URL.Query()["hitung"]
    if !ok || len(hitung_s[0]) < 1 {
        log.Println("Url Param 'hitung' is missing")
        return
    }
    hitung := hitung_s[0]

    sisi_s, ok := r.URL.Query()["sisi"]
    if !ok || len(sisi_s[0]) < 1 {
        log.Println("Url Param 'sisi' is missing")
        return
    }
    sisi := sisi_s[0]
    int_sisi, err := strconv.Atoi(sisi)
    if err != nil {
        // handle error
        fmt.Println(err)
    }
    

    log.Println("Url Param 'sisi' is: " + string(sisi))

    var luasPersegi = func (sebuahChannelUntukMenampungHasilPerhitunganSoal4 chan<- int, sisi int) {
  sebuahChannelUntukMenampungHasilPerhitunganSoal4 <- (sisi*sisi)
}
    var kelilingPersegi = func (sebuahChannelUntukMenampungHasilPerhitunganSoal4 chan<- int, sisi int ) {
  sebuahChannelUntukMenampungHasilPerhitunganSoal4 <- (sisi*4)
}
    var ch2 = make(chan int)
    if hitung == "luas" {
    go luasPersegi(ch2, int_sisi)
 fmt.Println("luas persegi adalah :", <-ch2)
} else if hitung == "keliling" {
    go kelilingPersegi(ch2, int_sisi)
 fmt.Println("keliling persegi adalah :", <-ch2)
}
}


// Persegi Panjang---------------------------------------------------------------------------------
func persegiPanjang(w http.ResponseWriter, r *http.Request) {

    hitung_s, ok := r.URL.Query()["hitung"]
    if !ok || len(hitung_s[0]) < 1 {
        log.Println("Url Param 'hitung' is missing")
        return
    }
    hitung := hitung_s[0]

    panjang_s, ok := r.URL.Query()["panjang"]
    if !ok || len(panjang_s[0]) < 1 {
        log.Println("Url Param 'panjang' is missing")
        return
    }
    panjang := panjang_s[0]
    int_panjang, err := strconv.Atoi(panjang)
    if err != nil {
        // handle error
        fmt.Println(err)
    }

    lebar_s, ok := r.URL.Query()["lebar"]
    if !ok || len(lebar_s[0]) < 1 {
        log.Println("Url Param 'lebar' is missing")
        return
    }
    lebar := lebar_s[0]
    int_lebar, err := strconv.Atoi(lebar)
    if err != nil {
        // handle error
        fmt.Println(err)
    }

    log.Println("Url Param 'panjang' is: " + string(panjang))
    log.Println("Url Param 'lebar' is: " + string(lebar))
    log.Println("Url Param 'hitung' is: " + string(hitung))

    var luasPersegiPanjang = func (sebuahChannelUntukMenampungHasilPerhitunganSoal4 chan<- int, panjang int, lebar int) {
  sebuahChannelUntukMenampungHasilPerhitunganSoal4 <- (panjang*lebar)
}
    var kelilingPersegiPanjang = func (sebuahChannelUntukMenampungHasilPerhitunganSoal4 chan<- int, panjang int, lebar int) {
  sebuahChannelUntukMenampungHasilPerhitunganSoal4 <- ((panjang*2)+(lebar*2))
}
    var ch3 = make(chan int)
    if hitung == "luas" {
    go luasPersegiPanjang(ch3, int_panjang,int_lebar)
 fmt.Println("luas persegi panjang adalah :", <-ch3)
} else if hitung == "keliling" {
    go kelilingPersegiPanjang(ch3, int_panjang,int_lebar)
 fmt.Println("keliling persegi panjang adalah :", <-ch3)
}
}

//Lingkaran---------------------------------------------------------------------------------
func lingkaran(w http.ResponseWriter, r *http.Request) {

    hitung_s, ok := r.URL.Query()["hitung"]
    if !ok || len(hitung_s[0]) < 1 {
        log.Println("Url Param 'hitung' is missing")
        return
    }
    hitung := hitung_s[0]

    jariJari_s, ok := r.URL.Query()["jariJari"]
    if !ok || len(jariJari_s[0]) < 1 {
        log.Println("Url Param 'jariJari' is missing")
        return
    }
    jariJari := jariJari_s[0]
    int_jariJari, err := strconv.Atoi(jariJari)
    if err != nil {
        // handle error
        fmt.Println(err)
    }
    

    log.Println("Url Param 'jariJari' is: " + string(jariJari))

    var luasLingkaran = func (sebuahChannelUntukMenampungHasilPerhitunganSoal4 chan<- float64, jariJari float64) {
  sebuahChannelUntukMenampungHasilPerhitunganSoal4 <- (math.Pi*jariJari*jariJari)
}
    var kelilingLingkaran = func (sebuahChannelUntukMenampungHasilPerhitunganSoal4 chan<- float64, jariJari float64 ) {
  sebuahChannelUntukMenampungHasilPerhitunganSoal4 <- (math.Pi*jariJari*2)
}
    var ch4 = make(chan float64)
    if hitung == "luas" {
    go luasLingkaran(ch4, float64(int_jariJari))
 fmt.Println("luas Lingkaran adalah :", <-ch4)
} else if hitung == "keliling" {
    go kelilingLingkaran(ch4, float64(int_jariJari))
 fmt.Println("keliling Lingkaran adalah :", <-ch4)
}
}

// Jajar Genjang---------------------------------------------------------------------------------
func jajarGenjang(w http.ResponseWriter, r *http.Request) {

    hitung_s, ok := r.URL.Query()["hitung"]
    if !ok || len(hitung_s[0]) < 1 {
        log.Println("Url Param 'hitung' is missing")
        return
    }
    hitung := hitung_s[0]

    sisi_s, ok := r.URL.Query()["sisi"]
    if !ok || len(sisi_s[0]) < 1 {
        log.Println("Url Param 'sisi' is missing")
        return
    }
    sisi := sisi_s[0]
    int_sisi, err := strconv.Atoi(sisi)
    if err != nil {
        // handle error
        fmt.Println(err)
    }

    alas_s, ok := r.URL.Query()["alas"]
    if !ok || len(alas_s[0]) < 1 {
        log.Println("Url Param 'alas' is missing")
        return
    }
    alas := alas_s[0]
    int_alas, err := strconv.Atoi(alas)
    if err != nil {
        // handle error
        fmt.Println(err)
    }

    tinggi_s, ok := r.URL.Query()["tinggi"]
    if !ok || len(tinggi_s[0]) < 1 {
        log.Println("Url Param 'tinggi' is missing")
        return
    }
    tinggi := tinggi_s[0]
    int_tinggi, err := strconv.Atoi(tinggi)
    if err != nil {
        // handle error
        fmt.Println(err)
    }

    log.Println("Url Param 'sisi' is: " + string(sisi))
    log.Println("Url Param 'alas' is: " + string(alas))
    log.Println("Url Param 'tinggi' is: " + string(tinggi))
    log.Println("Url Param 'hitung' is: " + string(hitung))

    var luasJajarGenjang = func (sebuahChannelUntukMenampungHasilPerhitunganSoal4 chan<- int, alas int, tinggi int) {
  sebuahChannelUntukMenampungHasilPerhitunganSoal4 <- (alas*tinggi)
}
    var kelilingJajarGenjang = func (sebuahChannelUntukMenampungHasilPerhitunganSoal4 chan<- int, sisi int, alas int) {
  sebuahChannelUntukMenampungHasilPerhitunganSoal4 <- ((alas*2)+(sisi*2))
}
    var ch5 = make(chan int)
    if hitung == "luas" {
    go luasJajarGenjang(ch5, int_alas,int_tinggi)
 fmt.Println("luas jajar genjang adalah :", <-ch5)
} else if hitung == "keliling" {
    go kelilingJajarGenjang(ch5, int_sisi, int_alas)
 fmt.Println("keliling jajar genjang adalah :", <-ch5)
}
}




