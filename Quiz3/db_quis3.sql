/*
SQLyog Ultimate v12.5.1 (32 bit)
MySQL - 10.1.38-MariaDB : Database - db_quis3
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_quis3` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_quis3`;

/*Table structure for table `book` */

DROP TABLE IF EXISTS `book`;

CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `release_year` int(11) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `total_page` int(11) DEFAULT NULL,
  `thickness` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `book_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `book` */

insert  into `book`(`id`,`title`,`description`,`image_url`,`release_year`,`price`,`total_page`,`thickness`,`created_at`,`updated_at`,`category_id`) values 
(1,'Judul Buku 1','Deskripsi Judul Buku 1','image_url',2021,'Rp21.000',190,'1.5cm','2006-01-02 15:04:55','2006-01-02 15:04:05',1),
(2,'Judul Buku 2','Deskripsi Buku 2','Url Buku 2',2005,'Rp22.000',302,'Tebal','2021-12-18 16:04:11','2021-12-18 16:04:11',1),
(4,'Judul Buku 3','Deskripsi Buku 3','http://www.google.com/url_buku_3',2005,'Rp22.000',302,'Tebal','2021-12-18 16:11:08','2021-12-18 16:11:08',1);

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `category` */

insert  into `category`(`id`,`name`,`created_at`,`updated_at`) values 
(1,'Kategori 1','2006-01-02 15:04:45','2006-01-02 15:04:05'),
(2,'Kategori 2','2021-12-18 15:10:15','2021-12-18 15:10:15');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
