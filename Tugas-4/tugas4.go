package main

import (
	"fmt";
	"strconv"
)

func main() {

	//soal 1
	for i := 1; i <= 20; i++ {
    if i % 3 == 0 && i % 2 != 0 {
    	fmt.Println(strconv.Itoa(i) + " - I Love Coding")
    } else if i % 2 == 0 {
    	fmt.Println(strconv.Itoa(i) + " - Berkualitas")
	} else if i % 2 != 0 {
    	fmt.Println(strconv.Itoa(i) + " - Santai")
	}
	}



	// line break kosong ini untuk merapihkan jawaban.
	fmt.Println("")
	fmt.Println("")
	//soal 2
	var pagar string
	for i := 1; i <= 7; i++ {
		pagar += "#"
		fmt.Println(pagar)
	}



	fmt.Println("")
	fmt.Println("")
	//soal 3
	var kalimat = [...]string{"aku", "dan", "saya", "sangat", "senang", "belajar", "golang"}
	fmt.Println(kalimat[2:7])



	fmt.Println("")
	fmt.Println("")
	//soal 4
	var sayuran = []string{}
	var angka int = 0
	sayuran = append(sayuran, "Bayam","Buncis","Kangkung","Kubis","Seledri","Tauge","Timun")
	for i := 0; i < len(sayuran); i++ {
		angka += 1
		fmt.Println(strconv.Itoa(angka) + ". " +sayuran[i])
	}

	

	fmt.Println("")
	fmt.Println("")
	//soal 5
	var satuan = map[string]int{
  	"panjang": 7,
  	"lebar":   4,
  	"tinggi":  6,
	}
	var volumebalok int = 1

	for key, element := range satuan {
        fmt.Println(key, "=", element)
        volumebalok *= element
    }
    fmt.Println("Volume Balok = " + strconv.Itoa(volumebalok))





}