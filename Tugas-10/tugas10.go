package main

import (
  "fmt";
  "strconv";
  "errors";
  "time";
  "math";
  "flag";
)

//function soal 1
func functionSoal1(kalimat string, tahun int){
  fmt.Println("")
  fmt.Println("")
  fmt.Println("Soal 1")
  fmt.Println(kalimat)
  fmt.Println(tahun)
}

//function soal 2
func kelilingSegitigaSamaSisi(angka int, benar bool)  (kalimat string, err error ) {
    if benar && angka == 0 { 
    return "Maaf anda belum menginput sisi dari segitiga sama sisi",nil
  } else if (!benar) && angka == 0 {
    return "Maaf anda belum menginput sisi dari segitiga sama sisi", errors.New("Sisi yang dimasukkan tidak boleh 0")
  } else if benar {
    return "keliling segitiga sama sisinya dengan sisi " + strconv.Itoa(angka) + " adalah " + strconv.Itoa(angka*3) + " cm.", nil
  } else if (!benar) {
    return strconv.Itoa(angka),nil
  } 
  return "",nil
}

//function soal 3
func tambahAngka(angkanya int, alamatMemoriAngka *int) {
  *alamatMemoriAngka += angkanya
}
func cetakAngka(alamatMemoriAngka *int){
  fmt.Println("")
  fmt.Println("")
  fmt.Println("Soal 3")
  fmt.Println("Total angkanya adalah " + strconv.Itoa(*alamatMemoriAngka))
}

func main() {
// deklarasi variabel angka ini simpan di baris pertama func main
angka := 1

//soal 1
defer functionSoal1("Golang Backend Development", 2021)

//soal 2
kelilingSegitigaSamaSisi(4, true)
kelilingSegitigaSamaSisi(8, false)
kelilingSegitigaSamaSisi(0, true)
kelilingSegitigaSamaSisi(0, false)
fmt.Println("Soal 2")
fmt.Println(kelilingSegitigaSamaSisi(4, true))
fmt.Println(kelilingSegitigaSamaSisi(8, false))
fmt.Println(kelilingSegitigaSamaSisi(0, true))
fmt.Println(kelilingSegitigaSamaSisi(0, false))

//soal 3
defer cetakAngka(&angka)
tambahAngka(7, &angka)
tambahAngka(6, &angka)
tambahAngka(-1, &angka)
tambahAngka(9, &angka)

//soal 4
  fmt.Println("")
  fmt.Println("")
  fmt.Println("Soal 4")
var phones = []string{}
var tambahkanModelHp = func(alamatMemoriPhones *[]string, modelHp string) {
  *alamatMemoriPhones = append(*alamatMemoriPhones,modelHp)
}
tambahkanModelHp(&phones, "Xiaomi")
tambahkanModelHp(&phones, "Asus")
tambahkanModelHp(&phones, "IPhone")
tambahkanModelHp(&phones, "Samsung")
tambahkanModelHp(&phones, "Oppo")
tambahkanModelHp(&phones, "Realme")
tambahkanModelHp(&phones, "Vivo")
angka2 := 0
for i := 0; i < len(phones); i++ {
    angka2 += 1
    fmt.Println(strconv.Itoa(angka2) + ". " +phones[i])
    time.Sleep(1 * time.Second)
}

//soal 5
  fmt.Println("")
  fmt.Println("")
  fmt.Println("Soal 5")
var luasLingkaran = func(jariJari float64) {
   fmt.Print("Luas lingkaran dengan jari jari ")
   fmt.Print(jariJari)
   fmt.Print(" adalah ")
   fmt.Println(math.Round(math.Pi*jariJari*jariJari))
}
var kelilingLingkaran = func(jariJari float64) {
     fmt.Print("Keliling lingkaran dengan jari jari ")
   fmt.Print(jariJari)
   fmt.Print(" adalah ")
   fmt.Println(math.Round(math.Pi*jariJari*2))
}
luasLingkaran(7)
kelilingLingkaran(7)
luasLingkaran(10)
kelilingLingkaran(10)
luasLingkaran(15)
kelilingLingkaran(15)

//soal 6
  fmt.Println("")
  fmt.Println("")
  fmt.Println("Soal 6")
var panjang = flag.Int64("panjang", 2, "Masukkan nilai panjang :")
var lebar = flag.Int64("lebar", 5, "Masukkan nilai lebar :")
flag.Parse()
  fmt.Print("Luas Persegi Panjang : " )  
  fmt.Print((*panjang)*(*lebar))
  fmt.Print("\n")
  fmt.Print("Keliling Persegi Panjang :" )  
  fmt.Print((*panjang * 2)+(*lebar * 2))
}