package main

import (
	"fmt";
	"strings";
	"strconv"
)

func main() {

	//soal 1
	var bootcamp string = "Bootcamp"
	var digital string = "Digital"
	var skill string = "Skill"
	var sanbercode string = "Sanbercode"
	var golang string = "Golang"
        fmt.Println(bootcamp+" "+digital+" "+skill+" "+sanbercode+" "+golang)

    //soal 2
    halo := "Halo Dunia"
    halo =  strings.Replace(halo,"Dunia","Golang",1)
    fmt.Println(halo)

    //soal 3
    var kataPertama = "saya";
    var kataKedua = "senang";
    var kataKetiga = "belajar";
    var kataKeempat = "golang";
    fmt.Println(kataPertama+" "+strings.Title(kataKedua)+" "+strings.Replace(kataKetiga,"r","R",1)+" "+strings.ToUpper(kataKeempat))

    //soal 4
    var angkaPertama= "8";
    var angkaKedua= "5";
	var angkaKetiga= "6";
	var angkaKeempat = "7";
	var pertama, err1= strconv.Atoi(angkaPertama)
	var kedua, err2 = strconv.Atoi(angkaKedua)
	var ketiga, err3 = strconv.Atoi(angkaKetiga)
	var keempat, err4 = strconv.Atoi(angkaKeempat)
	if err1 == nil && err2 == nil && err3 == nil && err4 == nil { }
	fmt.Println(pertama+kedua+ketiga+keempat)

	//soal 5
	kalimat := "halo halo bandung"
	angka := 2021
	kalimat = strings.Replace(kalimat,"halo","Hi",2)
	stringangka := strconv.Itoa(angka)
	fmt.Println(`" `+kalimat+`" - ` +stringangka)
}