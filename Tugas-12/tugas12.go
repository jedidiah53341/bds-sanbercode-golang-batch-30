package main

import "encoding/json"
import "fmt"
import "math"
import "net/http"
import "strconv"

func main() {

//Menampilkan laman di browser
 http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintln(w, "jariJari : 7, tinggi: 10, volume : " + strconv.FormatFloat(volumTabungBerjari7DanTinggi10, 'f', 2, 64) + ", luas alas: " + strconv.FormatFloat(luasLingkaranBerjari7, 'f', 2, 64) + ", keliling alas: " + strconv.FormatFloat(kelilingLingkaranBerjari7, 'f', 2, 64))
  })
  
  fmt.Println("starting web server at http://localhost:8080/")

  http.ListenAndServe(":8080", nil)



}
