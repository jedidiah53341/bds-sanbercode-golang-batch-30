package main

import (
  "fmt"
  "sync"
  "strconv"
  "math"
  "time"
)

//function soal 1

func main(){
//soal 1
fmt.Println("Soal 1")
var printMerkSmartphone = func (wg *sync.WaitGroup){
var phones = []string{"Xiaomi", "Asus", "Iphone", "Samsung", "Oppo", "Realme", "Vivo"}
angka := 0
for i := 0; i < len(phones); i++ {
    angka += 1
    fmt.Println(strconv.Itoa(angka) + ". " +phones[i])
    time.Sleep(1 * time.Second)
}
wg.Done()
}
var wg1 sync.WaitGroup
wg1.Add(1)
go printMerkSmartphone(&wg1)
wg1.Wait()


//soal 2
fmt.Println("")
fmt.Println("")
fmt.Println("Soal 2")
var getMovies = func (saluranFilm chan<- string, filmFilm ...string) {
  for i, v := range filmFilm {
    saluranFilm <- strconv.Itoa(i+1) + ". " + v
}
  close(saluranFilm)
}
var movies = []string{"Harry Potter", "LOTR", "SpiderMan", "Logan", "Avengers", "Insidious", "Toy Story"}
moviesChannel := make(chan string)
go getMovies(moviesChannel, movies...)
fmt.Println("List Movies:")
for value := range moviesChannel {
  fmt.Println(value)
}

//soal 3
fmt.Println("")
fmt.Println("")
fmt.Println("Soal 3")
hasilPerhitunganSoal3 := make(chan float64, 3)
var luasLingkaran = func (sebuahChannelUntukMenampungHasilPerhitunganSoal3 chan<- float64, jariJari int) {
  sebuahChannelUntukMenampungHasilPerhitunganSoal3 <- (math.Pi*float64(jariJari)*float64(jariJari))
}
var kelilingLingkaran = func (sebuahChannelUntukMenampungHasilPerhitunganSoal3 chan<- float64, jariJari int) {
  sebuahChannelUntukMenampungHasilPerhitunganSoal3 <- (math.Pi*float64(jariJari)*2)
}
var volumeTabung = func (sebuahChannelUntukMenampungHasilPerhitunganSoal3 chan<- float64, jariJari int, tinggi int) {
  sebuahChannelUntukMenampungHasilPerhitunganSoal3 <- (math.Pi*float64(jariJari)*float64(jariJari)*float64(tinggi))
}

fmt.Println("jari - jari = 8, :")
go luasLingkaran(hasilPerhitunganSoal3, 8)
fmt.Println("luas lingkaran adalah :", <-hasilPerhitunganSoal3)
go kelilingLingkaran(hasilPerhitunganSoal3, 8)
fmt.Println("keliling lingkaran adalah :", <-hasilPerhitunganSoal3)
go volumeTabung(hasilPerhitunganSoal3, 8, 10)
fmt.Println("volume tabung adalah :", <-hasilPerhitunganSoal3)
fmt.Println("")

fmt.Println("jari - jari = 14, :")
go luasLingkaran(hasilPerhitunganSoal3, 14)
fmt.Println("luas lingkaran adalah :", <-hasilPerhitunganSoal3)
go kelilingLingkaran(hasilPerhitunganSoal3, 14)
fmt.Println("keliling lingkaran adalah :", <-hasilPerhitunganSoal3)
go volumeTabung(hasilPerhitunganSoal3, 14, 10)
fmt.Println("volume tabung adalah :", <-hasilPerhitunganSoal3)
fmt.Println("")

fmt.Println("jari - jari = 20, :")
go luasLingkaran(hasilPerhitunganSoal3, 20)
fmt.Println("luas lingkaran adalah :", <-hasilPerhitunganSoal3)
go kelilingLingkaran(hasilPerhitunganSoal3, 20)
fmt.Println("keliling lingkaran adalah :", <-hasilPerhitunganSoal3)
go volumeTabung(hasilPerhitunganSoal3, 20, 10)
fmt.Println("volume tabung adalah :", <-hasilPerhitunganSoal3)

//soal 4
fmt.Println("")
fmt.Println("")
fmt.Println("Soal 4")
var luasPersegiPanjang = func (sebuahChannelUntukMenampungHasilPerhitunganSoal4 chan<- int, panjangAlas int, lebarAlas int) {
  sebuahChannelUntukMenampungHasilPerhitunganSoal4 <- (panjangAlas*lebarAlas)
}
var kelilingPersegiPanjang = func (sebuahChannelUntukMenampungHasilPerhitunganSoal4 chan<- int, panjangAlas int, lebarAlas int) {
  sebuahChannelUntukMenampungHasilPerhitunganSoal4 <- ((panjangAlas*2)+(lebarAlas*2))
}
var volumeBalok = func (sebuahChannelUntukMenampungHasilPerhitunganSoal4 chan<- int, panjangAlas int, lebarAlas int, tinggiBalok int) {
  sebuahChannelUntukMenampungHasilPerhitunganSoal4 <- (panjangAlas*lebarAlas*tinggiBalok)
}

  var ch1 = make(chan int)
  go luasPersegiPanjang(ch1, 5, 2)

  var ch2 = make(chan int)
  go kelilingPersegiPanjang(ch2, 5, 2)

  var ch3 = make(chan int)
  go volumeBalok(ch3, 5, 2, 10)

  for i := 0; i < 3; i++ {
    select {
    case area := <-ch1:
      fmt.Printf("Luas Persegi Panjang adalah\t: %.2d \n", area)
    case boundaries := <-ch2:
      fmt.Printf("Keliling Persegi Panjang adalah\t: %d \n", boundaries)
    case volume := <-ch3:
      fmt.Printf("Volume Balok adalah\t: %d \n", volume)
    }
  }

}