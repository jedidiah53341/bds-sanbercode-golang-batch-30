package main

import (
	"fmt";
	"math";
	"strconv";
)


func main() {

//soal 1
var luasLingkaran float64 
var kelilingLingkaran float64 

// Cek nilai luas dan keliling lingkaran
fmt.Print("Luas lingkaran : ")
fmt.Print(luasLingkaran)
fmt.Println()
fmt.Print("Keliling lingkaran : ")
fmt.Print(kelilingLingkaran)

// Membuat fungsi untuk update luas dan keliling lingkaran
var updateLuasLingkaran = func(alamatMemoriLuasLingkaran *float64, alamatMemoriKelilingLingkaran *float64, jariJari float64) {
*alamatMemoriLuasLingkaran = math.Pi*(jariJari*jariJari)
*alamatMemoriKelilingLingkaran = math.Pi*(jariJari*2)
}

// Update dengan jari - jari = 4
updateLuasLingkaran(&luasLingkaran, &kelilingLingkaran, 4)

// Cek nilai luas dan keliling lingkaran yang baru
fmt.Println()
fmt.Print("Luas lingkaran : ")
fmt.Print(luasLingkaran)
fmt.Println()
fmt.Print("Keliling lingkaran : ")
fmt.Print(kelilingLingkaran)




//soal 2
fmt.Println("")
fmt.Println("")
var introduce = func(alamatMemoriSentence *string, nama string, jenisKelamin string, okupasi string, umur string) {
var sapaan string
	if jenisKelamin == "laki-laki" {
		sapaan = "Pak "
	} else if jenisKelamin == "perempuan" {
		sapaan = "Bu "
	}
	*alamatMemoriSentence = sapaan + nama + " adalah seorang " + okupasi + " yang berusia " + umur + " tahun."
}
var sentence string 
introduce(&sentence, "John", "laki-laki", "penulis", "30")
fmt.Println(sentence) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"
introduce(&sentence, "Sarah", "perempuan", "model", "28")
fmt.Println(sentence) // Menampilkan "Bu Sarah adalah seorang model yang berusia 28 tahun"





//soal 3
fmt.Println("")
fmt.Println("")
var buah = []string{}
var tambahkanBuah = func(alamatMemoriBuah *[]string, buah string) {
	*alamatMemoriBuah = append(*alamatMemoriBuah,buah)
}
tambahkanBuah(&buah, "Jeruk")
tambahkanBuah(&buah, "Semangka")
tambahkanBuah(&buah, "Mangga")
tambahkanBuah(&buah, "Strawberry")
tambahkanBuah(&buah, "Durian")
tambahkanBuah(&buah, "Manggis")
tambahkanBuah(&buah, "Alpukat")
var angka int = 0
for i := 0; i < len(buah); i++ {
		angka += 1
		fmt.Println(strconv.Itoa(angka) + ". " +buah[i])
}





//soal 4
fmt.Println("")
fmt.Println("")
var dataFilm = []map[string]string{}
var tambahDataFilm = func(judulfilm string, durasifilm string, genrefilm string, tahunrilis string, alamatMemoriDataFilm *[]map[string]string) {
	var film  = map[string]string{}
	film["genre"] = genrefilm
	film["jam"] = durasifilm
	film["tahun"] = tahunrilis
	film["title"] = judulfilm
	dataFilm = append(dataFilm, film)
    }
tambahDataFilm("LOTR", "2 jam", "action", "1999", &dataFilm)
tambahDataFilm("avenger", "2 jam", "action", "2019", &dataFilm)
tambahDataFilm("spiderman", "2 jam", "action", "2004", &dataFilm)
tambahDataFilm("juon", "2 jam", "horror", "2004", &dataFilm)

// isi dengan jawaban anda untuk menampilkan data
angka = 0
for _, item := range dataFilm {
	angka += 1
	fmt.Print(angka)
	fmt.Print(". title : ")
   	fmt.Print(item["title"])
   	fmt.Println()
   	fmt.Print("   duration : ")
   	fmt.Print(item["jam"])
   	fmt.Println()
   	fmt.Print("   genre : ")
   	fmt.Print(item["genre"])
   	fmt.Println()
   	fmt.Print("   year : ")
   	fmt.Print(item["tahun"])
   	fmt.Println()
   	fmt.Println()
	}




}