package main

import (
  "fmt";
  "strconv";
  . "Tugas-9/packagebaru";
)

func main() {

//soal 1
var sebuahSegitigaSamaSisi HitungBangunDatar
sebuahSegitigaSamaSisi = SegitigaSamaSisi{1,2}
  fmt.Println("===== Segitiga Sama Sisi :")
  fmt.Println("luas      :", sebuahSegitigaSamaSisi.Luas())
  fmt.Println("keliling  :", sebuahSegitigaSamaSisi.Keliling())
var sebuahPersegiPanjang HitungBangunDatar
sebuahPersegiPanjang = PersegiPanjang{1,2}
 fmt.Println("===== PersegiPanjang :")
  fmt.Println("luas      :", sebuahPersegiPanjang.Luas())
  fmt.Println("keliling  :", sebuahPersegiPanjang.Keliling())
var sebuahTabung HitungBangunRuang
sebuahTabung = Tabung{1,2}
  fmt.Println("===== Tabung :")
  fmt.Println("volume      :", sebuahTabung.Volume())
  fmt.Println("luas permukaan  :", sebuahTabung.LuasPermukaan())
var sebuahBalok HitungBangunRuang
sebuahBalok = Balok{1,2,3}
  fmt.Println("===== Balok :")
  fmt.Println("volume      :", sebuahBalok.Volume())
  fmt.Println("luas permukaan  :", sebuahBalok.LuasPermukaan())

//soal 2
fmt.Println()
fmt.Println()
var sebuahTelepon InterfaceTampilkanDataPhone
sebuahTelepon = Phone{"Samsung Galaxy Note 20", "Samsung Galaxy Note 20",2020,[]string{"Mystic Bronze","Mystic White","Mystic Black"}}
fmt.Println(sebuahTelepon.MethodTampilkanDataPhone())

//soal 3
fmt.Println()
fmt.Println()
fmt.Println(LuasPersegi(4, true))
fmt.Println(LuasPersegi(8, false))
fmt.Println(LuasPersegi(0, true))
fmt.Println(LuasPersegi(0, false))

//soal 4
fmt.Println()
fmt.Println()
var prefix interface{}= "hasil penjumlahan dari "
var kumpulanAngkaPertama interface{} = []int{6,8}
var kumpulanAngkaKedua interface{} = []int{12,14}
// tulis jawaban anda disini
fmt.Println(prefix.(string)+strconv.Itoa(kumpulanAngkaPertama.([]int)[0])+ " + " + strconv.Itoa(kumpulanAngkaPertama.([]int)[1]) + " + " + strconv.Itoa(kumpulanAngkaKedua.([]int)[0]) + " + " + strconv.Itoa(kumpulanAngkaKedua.([]int)[1]) + " = " + strconv.Itoa(kumpulanAngkaPertama.([]int)[0]+kumpulanAngkaPertama.([]int)[1]+kumpulanAngkaKedua.([]int)[0]+kumpulanAngkaKedua.([]int)[1]))

}