package packagebaru

import (
  "math";
  "strconv";
  "strings";
)

//Struct dan interface soal 1
type SegitigaSamaSisi struct{
  Alas, Tinggi int
}
type PersegiPanjang struct{
  Panjang, Lebar int
}
type Tabung struct{
  JariJari, Tinggi float64
}
type Balok struct{
  Panjang, Lebar, Tinggi int
}
type HitungBangunDatar interface{
  Luas() int
  Keliling() int
}
type HitungBangunRuang interface{
  Volume() float64
  LuasPermukaan() float64
}

func (p SegitigaSamaSisi) Luas() int {
  return p.Alas*p.Tinggi/2
}
func (p SegitigaSamaSisi) Keliling() int {
  return p.Alas*3
}
func (p PersegiPanjang) Luas() int {
  return p.Panjang*p.Lebar
}
func (p PersegiPanjang) Keliling() int {
  return (p.Panjang*2)+(p.Lebar*2)
}
func (p Tabung) Volume() float64 {
  return math.Pi*(p.JariJari*p.JariJari)*p.Tinggi
}
func (p Tabung) LuasPermukaan() float64 {
  return 2*math.Pi*p.JariJari*(p.JariJari+p.Tinggi)
}
func (p Balok) Volume() float64 {
  return float64(p.Panjang*p.Lebar*p.Tinggi)
}
func (p Balok) LuasPermukaan() float64 {
  return float64(2*((p.Panjang*p.Lebar)+(p.Panjang*p.Tinggi)+(p.Lebar*p.Tinggi)))
}

//Struct dan interface soal 2
type Phone struct{
   Name, Brand string
   Year int
   Colors []string
}
type InterfaceTampilkanDataPhone interface{
  MethodTampilkanDataPhone() string
}
func (p Phone) MethodTampilkanDataPhone() string {
  var data string
  data += "Name : " + p.Name + "\n"
  data += "Brand : " + p.Brand + "\n"
  data += "Year : " + strconv.Itoa(p.Year) + "\n"
  data += "Colors : "
  for _, item := range p.Colors {
    data += item + ", "
  }
  data += "titik"
  data = strings.Replace(data, ", titik","",1)
  return data
}

//function soal 3
func LuasPersegi(angka int, benar bool) interface{} {
  if benar && angka == 0 {
    return "Maaf, anda belum menginput sisi dari persegi."
  } else if (!benar) && angka == 0 {
    return nil
  } else if benar {
    return "Luas persegi dengan sisi 2 cm adalah " + strconv.Itoa(angka) + " cm."
  } else if (!benar) {
    return strconv.Itoa(angka)
  } 
  return nil
}

