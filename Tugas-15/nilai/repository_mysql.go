package nilai

import (
  "database/sql"
  "datamahasiswa/config"
  "datamahasiswa/models"
  "context"
  "fmt"
  "log"
  "errors"
)

const (
  table          = "nilai"
  layoutDateTime = "2006-01-02 15:04:05"
)

// GetAll
func GetAll(ctx context.Context) ([]models.Nilai, error) {
  var kumpulanNilai []models.Nilai
  db, err := config.MySQL()

  if err != nil {
    log.Fatal("Cant connect to MySQL", err)
  }

  queryText := fmt.Sprintf("SELECT * FROM %v", table)
  rowQuery, err := db.QueryContext(ctx, queryText)

  if err != nil {
    log.Fatal(err)
  }

  for rowQuery.Next() {
    var nilai models.Nilai
    if err = rowQuery.Scan(&nilai.Id,
      &nilai.Indeks,
      &nilai.Skor,
      &nilai.Mahasiswa_Id,
      &nilai.Mata_Kuliah_Id); err != nil {
      return nil, err
    }

    if err != nil {
      log.Fatal(err)
    }

    kumpulanNilai = append(kumpulanNilai, nilai)
  }
  return kumpulanNilai, nil
}

// Insert Nilai
func Insert(ctx context.Context, nilai models.Nilai) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't connect to MySQL", err)
  }

  //Nilai hanya boleh diisi maksimal dengan angka 100 :
    if nilai.Skor > 100 {
      log.Fatal("Maaf, anda perlu memasukkan nilai dalam rentang 0 hingga 100.")
      return nil
    }
  
  nilai.Indeks = ""
    if nilai.Skor >= 80 {
      nilai.Indeks = "A"
    } else if nilai.Skor >= 70 {
      nilai.Indeks = "B"
    } else if nilai.Skor >= 60 {
      nilai.Indeks = "C"
    } else if nilai.Skor >= 50 {
      nilai.Indeks = "D"
    } else if nilai.Skor < 50 {
      nilai.Indeks = "E"
    }

  queryText := fmt.Sprintf("INSERT INTO %v (indeks, skor,mahasiswa_id, mata_kuliah_id) values('%v',%v,%v,%v)", table,
    nilai.Indeks,
    nilai.Skor,
    nilai.Mahasiswa_Id,
    nilai.Mata_Kuliah_Id,
  )
  _, err = db.ExecContext(ctx, queryText)

  if err != nil {
    return err
  }
  return nil
}

// Update Nilai
func Update(ctx context.Context, nilai models.Nilai, id string) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't connect to MySQL", err)
  }

  queryText := fmt.Sprintf("UPDATE %v set indeks ='%s', skor =%d, mahasiswa_id = %d, mata_kuliah_id = %d where id = %s",
    table,
    nilai.Indeks,
    nilai.Skor,
    nilai.Mahasiswa_Id,
    nilai.Mata_Kuliah_Id,
    id,
  )

  _, err = db.ExecContext(ctx, queryText)
  if err != nil {
    return err
  }

  return nil
}

// Delete Nilai
func Delete(ctx context.Context, id string) error {
    db, err := config.MySQL()

    if err != nil {
        log.Fatal("Can't connect to MySQL", err)
    }

    queryText := fmt.Sprintf("DELETE FROM %v where id = %s", table, id)

    s, err := db.ExecContext(ctx, queryText)

    if err != nil && err != sql.ErrNoRows {
        return err
    }

    check, err := s.RowsAffected()
    fmt.Println(check)
    if check == 0 {
        return errors.New("id tidak ada")
    }

    if err != nil {
        fmt.Println(err.Error())
    }

    return nil
}