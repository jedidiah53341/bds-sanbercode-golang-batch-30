package mata_Kuliah

import (
  "database/sql"
  "datamahasiswa/config"
  "datamahasiswa/models"
  "context"
  "fmt"
  "log"
  "errors"
)

const (
  table          = "mata_kuliah"
  layoutDateTime = "2006-01-02 15:04:05"
)

// GetAll
func GetAll(ctx context.Context) ([]models.Mata_Kuliah, error) {
  var kumpulanMata_Kuliah []models.Mata_Kuliah
  db, err := config.MySQL()

  if err != nil {
    log.Fatal("Cant connect to MySQL", err)
  }

  queryText := fmt.Sprintf("SELECT * FROM %v", table)
  rowQuery, err := db.QueryContext(ctx, queryText)

  if err != nil {
    log.Fatal(err)
  }

  for rowQuery.Next() {
    var mata_kuliah models.Mata_Kuliah
    if err = rowQuery.Scan(&mata_kuliah.Id,
      &mata_kuliah.Nama); err != nil {
      return nil, err
    }

    if err != nil {
      log.Fatal(err)
    }

    kumpulanMata_Kuliah = append(kumpulanMata_Kuliah, mata_kuliah)
  }
  return kumpulanMata_Kuliah, nil
}

// Insert mata_kuliah
func Insert(ctx context.Context, mata_kuliah models.Mata_Kuliah) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't connect to MySQL", err)
  }


  queryText := fmt.Sprintf("INSERT INTO %v (nama) values('%v')", table,
    mata_kuliah.Nama,
  )
  _, err = db.ExecContext(ctx, queryText)

  if err != nil {
    return err
  }
  return nil
}

// Update mata_kuliah
func Update(ctx context.Context, mata_kuliah models.Mata_Kuliah, id string) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't connect to MySQL", err)
  }

  queryText := fmt.Sprintf("UPDATE %v set nama ='%s' where id = %s",
    table,
    mata_kuliah.Nama,
    id,
  )

  _, err = db.ExecContext(ctx, queryText)
  if err != nil {
    return err
  }

  return nil
}

// Delete mata_kuliah
func Delete(ctx context.Context, id string) error {
    db, err := config.MySQL()

    if err != nil {
        log.Fatal("Can't connect to MySQL", err)
    }

    queryText := fmt.Sprintf("DELETE FROM %v where id = %s", table, id)

    s, err := db.ExecContext(ctx, queryText)

    if err != nil && err != sql.ErrNoRows {
        return err
    }

    check, err := s.RowsAffected()
    fmt.Println(check)
    if check == 0 {
        return errors.New("id tidak ada")
    }

    if err != nil {
        fmt.Println(err.Error())
    }

    return nil
}