package models

type (
  // Nilai
  Nilai struct {
  Id uint
  Indeks string `json:"indeks"`
  Skor int `json:"skor"`
  Mahasiswa_Id uint `json:"mahasiswa_id"`
  Mata_Kuliah_Id uint `json:"mata_kuliah_id"`
}
)