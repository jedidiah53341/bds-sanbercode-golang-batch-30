package main

import (
  "datamahasiswa/nilai"
  "datamahasiswa/mata_Kuliah"
  "datamahasiswa/mahasiswa"
  "datamahasiswa/utils"
  "datamahasiswa/models"
  "context"
  "fmt"
  "log" 
  "net/http"
  "encoding/json"
  "github.com/julienschmidt/httprouter"
)


func main() {
  router := httprouter.New()

  // Route untuk CRUD table nilai1
  router.GET("/nilai", GetDataNilai)
  router.POST("/nilai/tambah", PostDataNilai)
  router.PUT("/nilai/update/:id", UpdateDataNilai)
  router.DELETE("/nilai/hapus/:id", DeleteDataNilai)

  // Route untuk CRUD table Mahasiswa
  router.GET("/mahasiswa", GetDataMahasiswa)
  router.POST("/mahasiswa/tambah", PostDataMahasiswa)
  router.PUT("/mahasiswa/update/:id", UpdateDataMahasiswa)
  router.DELETE("/mahasiswa/hapus/:id", DeleteDataMahasiswa)

  // Route untuk CRUD table mata_Kuliah
  router.GET("/mata_Kuliah", GetDatamata_Kuliah)
  router.POST("/mata_Kuliah/tambah", PostDatamata_Kuliah)
  router.PUT("/mata_Kuliah/update/:id", UpdateDatamata_Kuliah)
  router.DELETE("/mata_Kuliah/hapus/:id", DeleteDatamata_Kuliah)

  fmt.Println("Server Running at http://localhost:8080")
  log.Fatal(http.ListenAndServe(":8080", router))

}

// Function Nilai Mulai ----------------------------------------------------------------------------------------
// Create nilai
func GetDataNilai(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  kumpulanNilai, err := nilai.GetAll(ctx)

  if err != nil {
    fmt.Println(err)
  }

  utils.ResponseJSON(w, kumpulanNilai, http.StatusOK)
}

// Post Nilai
func PostDataNilai(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
  if r.Header.Get("Content-Type") != "application/json" {
    http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
    return
  }
  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  var nilai1 models.Nilai
  if err := json.NewDecoder(r.Body).Decode(&nilai1); err != nil {
    utils.ResponseJSON(w, err, http.StatusBadRequest)
    return
  }
  if err := nilai.Insert(ctx, nilai1); err != nil {
    utils.ResponseJSON(w, err, http.StatusInternalServerError)
    return
  }
  res := map[string]string{
    "status": "Berhasil Menambahkan 1 item ke tabel Nilai",
  }
  utils.ResponseJSON(w, res, http.StatusCreated)

}

// Update Nilai
func UpdateDataNilai(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
  if r.Header.Get("Content-Type") != "application/json" {
    http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
    return
  }

  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()

  var nilai1 models.Nilai

  if err := json.NewDecoder(r.Body).Decode(&nilai1); err != nil {
    utils.ResponseJSON(w, err, http.StatusBadRequest)
    return
  }

  var idMovie = ps.ByName("id")

  if err := nilai.Update(ctx, nilai1, idMovie); err != nil {
    utils.ResponseJSON(w, err, http.StatusInternalServerError)
    return
  }

  res := map[string]string{
    "status": "Berhasil Mengupdate 1 item dengan id " + idMovie + " ke tabel Nilai",
  }

  utils.ResponseJSON(w, res, http.StatusCreated)
}

// Delete Nilai
func DeleteDataNilai(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  var idMovie = ps.ByName("id")
  if err := nilai.Delete(ctx, idMovie); err != nil {
    kesalahan := map[string]string{
      "error": fmt.Sprintf("%v", err),
    }
    utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
    return
  }
  res := map[string]string{
    "status": "Berhasil menghapus 1 item dengan id " + idMovie + " dari tabel Nilai",
  }
  utils.ResponseJSON(w, res, http.StatusOK)
}
// Function Nilai Selesai ----------------------------------------------------------------------------------------

// Function Mahasiswa Mulai ----------------------------------------------------------------------------------------
// Create Mahasiswa
func GetDataMahasiswa(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  kumpulanMahasiswa, err := mahasiswa.GetAll(ctx)

  if err != nil {
    fmt.Println(err)
  }

  utils.ResponseJSON(w, kumpulanMahasiswa, http.StatusOK)
}

// Post Mahasiswa
func PostDataMahasiswa(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
  if r.Header.Get("Content-Type") != "application/json" {
    http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
    return
  }
  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  var mahasiswa1 models.Mahasiswa
  if err := json.NewDecoder(r.Body).Decode(&mahasiswa1); err != nil {
    utils.ResponseJSON(w, err, http.StatusBadRequest)
    return
  }
  if err := mahasiswa.Insert(ctx, mahasiswa1); err != nil {
    utils.ResponseJSON(w, err, http.StatusInternalServerError)
    return
  }
  res := map[string]string{
    "status": "Berhasil Menambahkan 1 item ke tabel Mahasiswa",
  }
  utils.ResponseJSON(w, res, http.StatusCreated)

}

// Update Mahasiswa
func UpdateDataMahasiswa(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
  if r.Header.Get("Content-Type") != "application/json" {
    http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
    return
  }

  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()

  var mahasiswa1 models.Mahasiswa

  if err := json.NewDecoder(r.Body).Decode(&mahasiswa1); err != nil {
    utils.ResponseJSON(w, err, http.StatusBadRequest)
    return
  }

  var idMovie = ps.ByName("id")

  if err := mahasiswa.Update(ctx, mahasiswa1, idMovie); err != nil {
    utils.ResponseJSON(w, err, http.StatusInternalServerError)
    return
  }

  res := map[string]string{
    "status": "Berhasil Mengupdate 1 item dengan id " + idMovie + " ke tabel Mahasiswa",
  }

  utils.ResponseJSON(w, res, http.StatusCreated)
}

// Delete Mahasiswa
func DeleteDataMahasiswa(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  var idMovie = ps.ByName("id")
  if err := mahasiswa.Delete(ctx, idMovie); err != nil {
    kesalahan := map[string]string{
      "error": fmt.Sprintf("%v", err),
    }
    utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
    return
  }
  res := map[string]string{
    "status": "Berhasil menghapus 1 item dengan id " + idMovie + " dari tabel Mahasiswa",
  }
  utils.ResponseJSON(w, res, http.StatusOK)
}
// Function Mahasiswa Selesai ----------------------------------------------------------------------------------------

// Function Mata Kuliah Mulai ----------------------------------------------------------------------------------------
// Create Mata Kuliah
func GetDatamata_Kuliah(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  kumpulanmata_Kuliah, err := mata_Kuliah.GetAll(ctx)

  if err != nil {
    fmt.Println(err)
  }

  utils.ResponseJSON(w, kumpulanmata_Kuliah, http.StatusOK)
}

// Post Mata Kuliah
func PostDatamata_Kuliah(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
  if r.Header.Get("Content-Type") != "application/json" {
    http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
    return
  }
  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  var mata_Kuliah1 models.Mata_Kuliah
  if err := json.NewDecoder(r.Body).Decode(&mata_Kuliah1); err != nil {
    utils.ResponseJSON(w, err, http.StatusBadRequest)
    return
  }
  if err := mata_Kuliah.Insert(ctx, mata_Kuliah1); err != nil {
    utils.ResponseJSON(w, err, http.StatusInternalServerError)
    return
  }
  res := map[string]string{
    "status": "Berhasil Menambahkan 1 item ke tabel Mata Kuliah",
  }
  utils.ResponseJSON(w, res, http.StatusCreated)

}

// Update Mata Kuliah
func UpdateDatamata_Kuliah(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
  if r.Header.Get("Content-Type") != "application/json" {
    http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
    return
  }

  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()

  var mata_Kuliah1 models.Mata_Kuliah

  if err := json.NewDecoder(r.Body).Decode(&mata_Kuliah1); err != nil {
    utils.ResponseJSON(w, err, http.StatusBadRequest)
    return
  }

  var idMovie = ps.ByName("id")

  if err := mata_Kuliah.Update(ctx, mata_Kuliah1, idMovie); err != nil {
    utils.ResponseJSON(w, err, http.StatusInternalServerError)
    return
  }

  res := map[string]string{
    "status": "Berhasil Mengupdate 1 item dengan id " + idMovie + " ke tabel Mata Kuliah",
  }

  utils.ResponseJSON(w, res, http.StatusCreated)
}

// Delete Mata Kuliah
func DeleteDatamata_Kuliah(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
  ctx, cancel := context.WithCancel(context.Background())
  defer cancel()
  var idMovie = ps.ByName("id")
  if err := mata_Kuliah.Delete(ctx, idMovie); err != nil {
    kesalahan := map[string]string{
      "error": fmt.Sprintf("%v", err),
    }
    utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
    return
  }
  res := map[string]string{
    "status": "Berhasil menghapus 1 item dengan id " + idMovie + " dari tabel Mata Kuliah",
  }
  utils.ResponseJSON(w, res, http.StatusOK)
}
// Function Mata Kuliah Selesai ----------------------------------------------------------------------------------------