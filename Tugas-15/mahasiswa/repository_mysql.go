package mahasiswa

import (
  "database/sql"
  "datamahasiswa/config"
  "datamahasiswa/models"
  "context"
  "fmt"
  "log"
  "errors"
)

const (
  table          = "mahasiswa"
  layoutDateTime = "2006-01-02 15:04:05"
)

// GetAll
func GetAll(ctx context.Context) ([]models.Mahasiswa, error) {
  var kumpulanMahasiswa []models.Mahasiswa
  db, err := config.MySQL()

  if err != nil {
    log.Fatal("Cant connect to MySQL", err)
  }

  queryText := fmt.Sprintf("SELECT * FROM %v", table)
  rowQuery, err := db.QueryContext(ctx, queryText)

  if err != nil {
    log.Fatal(err)
  }

  for rowQuery.Next() {
    var mahasiswa models.Mahasiswa
    if err = rowQuery.Scan(&mahasiswa.Id,
      &mahasiswa.Nama); err != nil {
      return nil, err
    }

    if err != nil {
      log.Fatal(err)
    }

    kumpulanMahasiswa = append(kumpulanMahasiswa, mahasiswa)
  }
  return kumpulanMahasiswa, nil
}

// Insert Mahasiswa
func Insert(ctx context.Context, mahasiswa models.Mahasiswa) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't connect to MySQL", err)
  }


  queryText := fmt.Sprintf("INSERT INTO %v (nama) values('%v')", table,
    mahasiswa.Nama,
  )
  _, err = db.ExecContext(ctx, queryText)

  if err != nil {
    return err
  }
  return nil
}

// Update Mahasiswa
func Update(ctx context.Context, mahasiswa models.Mahasiswa, id string) error {
  db, err := config.MySQL()
  if err != nil {
    log.Fatal("Can't connect to MySQL", err)
  }

  queryText := fmt.Sprintf("UPDATE %v set nama ='%s' where id = %s",
    table,
    mahasiswa.Nama,
    id,
  )

  _, err = db.ExecContext(ctx, queryText)
  if err != nil {
    return err
  }

  return nil
}

// Delete Mahasiswa
func Delete(ctx context.Context, id string) error {
    db, err := config.MySQL()

    if err != nil {
        log.Fatal("Can't connect to MySQL", err)
    }

    queryText := fmt.Sprintf("DELETE FROM %v where id = %s", table, id)

    s, err := db.ExecContext(ctx, queryText)

    if err != nil && err != sql.ErrNoRows {
        return err
    }

    check, err := s.RowsAffected()
    fmt.Println(check)
    if check == 0 {
        return errors.New("id tidak ada")
    }

    if err != nil {
        fmt.Println(err.Error())
    }

    return nil
}