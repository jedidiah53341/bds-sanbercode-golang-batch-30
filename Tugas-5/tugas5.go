package main

import (
	"fmt";
	"strings";
	"strconv";
)

	// fungsi soal 1
	func luasPersegiPanjang(panjang int, lebar int) int {
  	return panjang*lebar
	}
	func kelilingPersegiPanjang(panjang int, lebar int) int {
  	return (panjang*2)+(lebar*2)
	}
	func volumeBalok(panjang int, lebar int, tinggi int) int {
  	return panjang*lebar*tinggi
	}

	//fungsi soal 2
	func introduce(nama string, jenis_kelamin string, okupasi string, umur int) (perkenalan string) {
	var sapaan string
	if jenis_kelamin == "laki-laki" {
		sapaan = "Pak "
	} else if jenis_kelamin == "perempuan" {
		sapaan = "Bu "
	}
  	perkenalan = sapaan + nama + " adalah seorang " + okupasi + " yang berusia " + strconv.Itoa(umur) + " tahun."
  	return
	}

	//fungsi soal 3
	func buahFavorit(nama string, buahfavorit ...string) string {
  	var perkenalan string = "Halo, nama saya " + nama + " dan buah favorit saya adalah "
  	for _, buah := range buahfavorit {
    perkenalan += `"` + buah + `", `
  	}
  	perkenalan += "titik"
  	perkenalan = strings.Replace(perkenalan, ", titik",".",1)
  	return perkenalan
	}


func main() {

	//soal 1
	panjang := 12
	lebar := 4
	tinggi := 8
  
	luas := luasPersegiPanjang(panjang, lebar)
	keliling := kelilingPersegiPanjang(panjang, lebar)
	volume := volumeBalok(panjang, lebar, tinggi)

	fmt.Println(luas) 
	fmt.Println(keliling)
	fmt.Println(volume)

	// line break kosong ini untuk merapihkan jawaban.
	fmt.Println("")
	//soal 2
	john := introduce("John", "laki-laki", "penulis", 30)
	fmt.Println(john) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"

	sarah := introduce("Sarah", "perempuan", "model", 28)
	fmt.Println(sarah) // Menampilkan "Bu Sarah adalah seorang model yang berusia 28 tahun"

	// line break kosong ini untuk merapihkan jawaban.
	fmt.Println("")
	//soal 3
	var buah = []string{"semangka", "jeruk", "melon", "pepaya"}

	var buahFavoritJohn = buahFavorit("John", buah...)

	fmt.Println(buahFavoritJohn)
	// halo nama saya john dan buah favorit saya adalah "semangka", "jeruk", "melon", "pepaya"

	// line break kosong ini untuk merapihkan jawaban.
	fmt.Println("")
	//soal 4
	var dataFilm = []map[string]string{}

	var tambahDataFilm = func(judulfilm string, durasifilm string, genrefilm string, tahunrilis int) {
	var film  = map[string]string{}
	film["genre"] = genrefilm
	film["jam"] = durasifilm
	film["tahun"] = strconv.Itoa(tahunrilis)
	film["title"] = judulfilm
	dataFilm = append(dataFilm, film)
    }

	tambahDataFilm("LOTR", "2 jam", "action", 1999)
	tambahDataFilm("avenger", "2 jam", "action", 2019)
	tambahDataFilm("spiderman", "2 jam", "action", 2004)
	tambahDataFilm("juon", "2 jam", "horror", 2004)

	for _, item := range dataFilm {
   	fmt.Println(item)
	}




}