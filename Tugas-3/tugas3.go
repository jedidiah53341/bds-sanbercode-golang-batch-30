package main

import (
	"fmt";
	"strconv"
)

func main() {

//soal 1
var panjangPersegiPanjang string = "8"
var lebarPersegiPanjang string = "5"
var alasSegitiga string = "6"
var tinggiSegitiga string = "7"

int_panjangPersegiPanjang, err1 := strconv.Atoi(panjangPersegiPanjang)
int_lebarPersegiPanjang , err2 := strconv.Atoi(lebarPersegiPanjang)
int_alasSegitiga , err3 := strconv.Atoi(alasSegitiga)
int_tinggiSegitiga , err4 := strconv.Atoi(tinggiSegitiga)
if err1 == nil && err2 == nil && err3 == nil && err4 == nil {
	var luasPersegiPanjang int = int_panjangPersegiPanjang*int_lebarPersegiPanjang
	var kelilingPersegiPanjang int = (int_panjangPersegiPanjang*2)+(int_lebarPersegiPanjang*2)
	var luasSegitiga int = int_alasSegitiga*int_tinggiSegitiga/2

	fmt.Println(luasPersegiPanjang)
	fmt.Println(kelilingPersegiPanjang)
	fmt.Println(luasSegitiga)
}

//soal 2
var nilaiJohn = 80
var nilaiDoe = 50
if nilaiJohn >= 80 {fmt.Println("Indeks nilai John adalah A.")
} else if nilaiJohn >= 70 {fmt.Println("Indeks nilai John adalah B.")
} else if nilaiJohn >= 60 {fmt.Println("Indeks nilai John adalah C.")
} else if nilaiJohn >= 50 {fmt.Println("Indeks nilai John adalah D.")
} else {fmt.Println("Indeks nilai John adalah E.")}
if nilaiDoe >= 80 {fmt.Println("Indeks nilai Doe adalah A.")
} else if nilaiDoe >= 70 {fmt.Println("Indeks nilai Doe adalah B.")
} else if nilaiDoe >= 60 {fmt.Println("Indeks nilai Doe adalah C.")
} else if nilaiDoe >= 50 {fmt.Println("Indeks nilai Doe adalah D.")
} else {fmt.Println("Indeks nilai Doe adalah E.")}

//soal 3
var tanggal = 11;
var bulan = 6; 
var tahun = 1999;

string_tanggal:= strconv.Itoa(tanggal)
var string_bulan string
string_tahun := strconv.Itoa(tahun)
switch {
case bulan == 1:
	string_bulan = "Januari"
case bulan == 2:
	string_bulan = "Februari"
case bulan == 3:
	string_bulan = "Maret"
case bulan == 4:
	string_bulan = "April"
case bulan == 5:
	string_bulan = "Mei"
case bulan == 6:
	string_bulan = "Juni"
case bulan == 7:
	string_bulan = "Juli"
case bulan == 8:
	string_bulan = "Agustus"
case bulan == 9:
	string_bulan = "September"
case bulan == 10:
	string_bulan = "Oktober"
case bulan == 11:
	string_bulan = "November"
case bulan == 12:
	string_bulan = "Desember"
}
fmt.Println(string_tanggal+" "+string_bulan+" "+string_tahun)

//soal 4
if tahun <= 1964 {fmt.Println("Saya tergolong Baby Boomer.")
} else if tahun <= 1979 {fmt.Println("Saya tergolong Generasi X.")
} else if tahun <= 1994 {fmt.Println("Saya tergolong Generasi Y (Millenials).")
} else if tahun <= 2015 {fmt.Println("Saya tergolong Generasi Z.")}

}