package main

import (
  "encoding/json"
  "fmt"
  "log"
  "net/http"
  "strconv"
  "time"
)

type NilaiMahasiswa struct{
  Nama string `json:"nama"`
  MataKuliah string `json:"matakuliah"`
  IndeksNilai string
  Nilai uint `json:"nilai"`
  ID uint
}

var NilaiNilaiMahasiswa = []NilaiMahasiswa{
  {"Jedidiah","Fisika","A",85,0},
}

//Function untuk GET nilai-nilai mahasiswa
func dapatkanNilaiNilaiMahasiswa(w http.ResponseWriter, r *http.Request) {
  if r.Method == "GET" {
    dataMovies, err := json.Marshal(NilaiNilaiMahasiswa)
    
    if err != nil {
      http.Error(w, err.Error(), http.StatusInternalServerError)
    }

    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(http.StatusOK)
    w.Write(dataMovies)
    return
  }
  http.Error(w, "ERROR....", http.StatusNotFound)
}

//Function untuk POST sebuah nilai mahasiswa :
func postNilai(w http.ResponseWriter, r *http.Request) {
  w.Header().Set("Content-Type", "application/json")
  var Data NilaiMahasiswa
  if r.Method == "POST" {
    if r.Header.Get("Content-Type") == "application/json" {
      // parse dari json
      decodeJSON := json.NewDecoder(r.Body)
      if err := decodeJSON.Decode(&Data); err != nil {
        log.Fatal(err)
      }
    } else {
      // parse dari form
    nama := r.PostFormValue("nama")
    matakuliah := r.PostFormValue("matakuliah")
    dapatkanNilai := r.PostFormValue("nilai")
    nilai, _ := strconv.Atoi(dapatkanNilai)

    //Nilai hanya boleh diisi maksimal dengan angka 100 :
    if nilai > 100 {
      w.Write([]byte("Maaf, anda perlu memasukkan nilai dalam rentang 0 hingga 100."))
      return
    }

    uint_nilai := uint(nilai)

    //Menentukan index nilai :
    indexnilai := ""
    if nilai >= 80 {
      indexnilai = "A"
    } else if nilai >= 70 {
      indexnilai = "B"
    } else if nilai >= 60 {
      indexnilai = "C"
    } else if nilai >= 50 {
      indexnilai = "D"
    } else if nilai < 50 {
      indexnilai = "E"
    }

    //Membuat angka random untuk ID :
    int_id:=time.Now().UnixNano()/(1<<3)
    uint_id := uint(int_id)

      Data = NilaiMahasiswa{
         Nama:    nama,
         MataKuliah: matakuliah,
         IndeksNilai: indexnilai,
         Nilai: uint_nilai,
         ID: uint_id,
      }
      NilaiNilaiMahasiswa = append(NilaiNilaiMahasiswa, Data)
    }

    data2, _ := json.Marshal(Data) // to byte
    w.Write(data2)                // cetak di browser
    return
  }

  http.Error(w, "NOT FOUND", http.StatusNotFound)
  return
}

//Simple Auth Middleware dengan username 'admin' dan password 'admin'
func Auth(next http.Handler) http.Handler {
  return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
    uname, pwd, ok := r.BasicAuth()
    if !ok {
      w.Write([]byte("Username atau Password tidak boleh kosong"))
      return
    }

    if uname == "admin" && pwd == "admin" {
      next.ServeHTTP(w, r)
      return
   }
    w.Write([]byte("Username atau Password tidak sesuai"))
    return
  })
}

func main() {
  //Route untuk mendapatkan nilai mahasiswa :
  http.HandleFunc("/", dapatkanNilaiNilaiMahasiswa)

  //Route untuk POST sebuah nilai mahasiswa :
  http.Handle("/post_nilai", Auth(http.HandlerFunc(postNilai)))

  fmt.Println("server running at http://localhost:8080")
  if err := http.ListenAndServe(":8080", nil); err != nil {
    log.Fatal(err)
  }
}