package main

import (
	"fmt";
  "math";
  "strconv";
  "strings";
)

//Struct dan interface soal 1
type segitigaSamaSisi struct{
  alas, tinggi int
}
type persegiPanjang struct{
  panjang, lebar int
}
type tabung struct{
  jariJari, tinggi float64
}
type balok struct{
  panjang, lebar, tinggi int
}
type hitungBangunDatar interface{
  luas() int
  keliling() int
}
type hitungBangunRuang interface{
  volume() float64
  luasPermukaan() float64
}

func (p segitigaSamaSisi) luas() int {
  return p.alas*p.tinggi/2
}
func (p segitigaSamaSisi) keliling() int {
  return p.alas*3
}
func (p persegiPanjang) luas() int {
  return p.panjang*p.lebar
}
func (p persegiPanjang) keliling() int {
  return (p.panjang*2)+(p.lebar*2)
}
func (p tabung) volume() float64 {
  return math.Pi*(p.jariJari*p.jariJari)*p.tinggi
}
func (p tabung) luasPermukaan() float64 {
  return 2*math.Pi*p.jariJari*(p.jariJari+p.tinggi)
}
func (p balok) volume() float64 {
  return float64(p.panjang*p.lebar*p.tinggi)
}
func (p balok) luasPermukaan() float64 {
  return float64(2*((p.panjang*p.lebar)+(p.panjang*p.tinggi)+(p.lebar*p.tinggi)))
}

//Struct dan interface soal 2
type phone struct{
   name, brand string
   year int
   colors []string
}
type interfaceTampilkanDataPhone interface{
  MethodTampilkanDataPhone() string
}
func (p phone) MethodTampilkanDataPhone() string {
  var data string
  data += "name : " + p.name + "\n"
  data += "brand : " + p.brand + "\n"
  data += "year : " + strconv.Itoa(p.year) + "\n"
  data += "colors : "
  for _, item := range p.colors {
    data += item + ", "
  }
  data += "titik"
  data = strings.Replace(data, ", titik","",1)
  return data
}

//function soal 3
func luasPersegi(angka int, benar bool) interface{} {
  if benar && angka == 0 {
    return "Maaf, anda belum menginput sisi dari persegi."
  } else if (!benar) && angka == 0 {
    return nil
  } else if benar {
    return "Luas persegi dengan sisi 2 cm adalah " + strconv.Itoa(angka) + " cm."
  } else if (!benar) {
    return strconv.Itoa(angka)
  } 
  return nil
}



func main() {

//soal 1
var sebuahSegitigaSamaSisi hitungBangunDatar
sebuahSegitigaSamaSisi = segitigaSamaSisi{1,2}
  fmt.Println("===== Segitiga Sama Sisi :")
  fmt.Println("luas      :", sebuahSegitigaSamaSisi.luas())
  fmt.Println("keliling  :", sebuahSegitigaSamaSisi.keliling())
var sebuahPersegiPanjang hitungBangunDatar
sebuahPersegiPanjang = persegiPanjang{1,2}
 fmt.Println("===== PersegiPanjang :")
  fmt.Println("luas      :", sebuahPersegiPanjang.luas())
  fmt.Println("keliling  :", sebuahPersegiPanjang.keliling())
var sebuahTabung hitungBangunRuang
sebuahTabung = tabung{1,2}
  fmt.Println("===== Tabung :")
  fmt.Println("volume      :", sebuahTabung.volume())
  fmt.Println("luas permukaan  :", sebuahTabung.luasPermukaan())
var sebuahBalok hitungBangunRuang
sebuahBalok = balok{1,2,3}
  fmt.Println("===== Balok :")
  fmt.Println("volume      :", sebuahBalok.volume())
  fmt.Println("luas permukaan  :", sebuahBalok.luasPermukaan())

//soal 2
fmt.Println()
fmt.Println()
var sebuahTelepon interfaceTampilkanDataPhone
sebuahTelepon = phone{"Samsung Galaxy Note 20", "Samsung Galaxy Note 20",2020,[]string{"Mystic Bronze","Mystic White","Mystic Black"}}
fmt.Println(sebuahTelepon.MethodTampilkanDataPhone())

//soal 3
fmt.Println()
fmt.Println()
fmt.Println(luasPersegi(4, true))
fmt.Println(luasPersegi(8, false))
fmt.Println(luasPersegi(0, true))
fmt.Println(luasPersegi(0, false))

//soal 4
fmt.Println()
fmt.Println()
var prefix interface{}= "hasil penjumlahan dari "
var kumpulanAngkaPertama interface{} = []int{6,8}
var kumpulanAngkaKedua interface{} = []int{12,14}
// tulis jawaban anda disini
fmt.Println(prefix.(string)+strconv.Itoa(kumpulanAngkaPertama.([]int)[0])+ " + " + strconv.Itoa(kumpulanAngkaPertama.([]int)[1]) + " + " + strconv.Itoa(kumpulanAngkaKedua.([]int)[0]) + " + " + strconv.Itoa(kumpulanAngkaKedua.([]int)[1]) + " = " + strconv.Itoa(kumpulanAngkaPertama.([]int)[0]+kumpulanAngkaPertama.([]int)[1]+kumpulanAngkaKedua.([]int)[0]+kumpulanAngkaKedua.([]int)[1]))

}