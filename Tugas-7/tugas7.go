package main

import (
	"fmt";
)

//Struct soal 1
type buah struct {
    nama string
    warna string
    adaBijinya bool
    harga int
}

//Struct dan method soal 2
type segitiga struct{
  alas, tinggi int
}
type persegi struct{
  sisi int
}
type persegiPanjang struct{
  panjang, lebar int
}
func (s segitiga) luasSegitiga() int {
  return s.alas * s.tinggi/2
}
func (s persegi) luasPersegi() int {
  return s.sisi * s.sisi
}
func (s persegiPanjang) luasPersegiPanjang() int {
  return s.panjang * s.lebar
}

//Struct dan method soal 3
type phone struct{
   name, brand string
   year int
   colors []string
}
func (s phone) masukkanWarna(alamatMemoriObjekPhone *[]string,warna string) {
	*alamatMemoriObjekPhone = append(*alamatMemoriObjekPhone,warna)
}
//Struct dan method soal 4
type movie struct{
   title, genre string
   duration, year int
}
func tambahDataFilm(judulfilm string, durasifilm int, genrefilm string, tahunrilis int,alamatMemoriDataFilm *[]movie) {
	var film movie
	film.genre = genrefilm
	film.duration = durasifilm
	film.year = tahunrilis
	film.title = judulfilm
	*alamatMemoriDataFilm = append(*alamatMemoriDataFilm, film)
    }


func main() {

//soal 1
var buahNanas = buah{"Nanas","Kuning",false,9000}
var buahJeruk = buah{"Jeruk","Oranye",true,8000}
var buahSemangka = buah{"Semangka","Hijau & Merah",true,10000}
var buahPisang = buah{"Pisang","Kuning",false,5000}
//print semua objek
fmt.Println(buahNanas)
fmt.Println(buahJeruk)
fmt.Println(buahSemangka)
fmt.Println(buahPisang)

//soal 2
fmt.Println("")
fmt.Println("")
//Membuat objek segitiga, persegi, dan persegi panjang :
var segitiga = segitiga{5,10}
var persegi = persegi{5}
var persegiPanjang = persegiPanjang{5,10}
//Menggunakan masing-masing method luas terhadap ketiga objek tersebut :
fmt.Println(segitiga.luasSegitiga())
fmt.Println(persegi.luasPersegi())
fmt.Println(persegiPanjang.luasPersegiPanjang())

//soal 3
fmt.Println("")
fmt.Println("")
//Membuat sebuah objek dari struct phone
var handphone1 phone
//Menggunakan method masukkanWarna untuk memasukan 2 warna ke slice 'colors'
handphone1.masukkanWarna(&handphone1.colors,"Putih")
handphone1.masukkanWarna(&handphone1.colors,"Hitam")
//Print objek 'handphone1'
fmt.Println(handphone1)

//soal 4
fmt.Println("")
fmt.Println("")
var dataFilm = []movie{}
tambahDataFilm("LOTR", 120, "action", 1999, &dataFilm)
tambahDataFilm("avenger", 120, "action", 2019, &dataFilm)
tambahDataFilm("spiderman", 120, "action", 2004, &dataFilm)
tambahDataFilm("juon", 120, "horror", 2004, &dataFilm)

// isi dengan jawaban anda untuk menampilkan data
angka := 0
for _, item := range dataFilm {
	angka += 1
	fmt.Print(angka)
	fmt.Print(". title : ")
   	fmt.Print(item.title)
   	fmt.Println()
   	fmt.Print("   duration : ")
   	fmt.Print(item.duration/60)
   	fmt.Print(" jam")
   	fmt.Println()
   	fmt.Print("   genre : ")
   	fmt.Print(item.genre)
   	fmt.Println()
   	fmt.Print("   year : ")
   	fmt.Print(item.year)
   	fmt.Println()
   	fmt.Println()
	}
}